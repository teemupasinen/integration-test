const { initParams } = require("request");

/**
 * padding outputs always two characters
 * @param {string} hex one or the characters
 * @returns {string} hex with two characters
 */

const pad = (hex) => {
  return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
  rgbToHex: (red, green, blue) => {
    const redHex = red.toString(16); // 0-255 -> 0-ff
    const greenHex = green.toString(16); // 0-255 -> 0-ff
    const blueHex = blue.toString(16); // 0-255 -> 0-ff

    return pad(redHex) + pad(greenHex) + pad(blueHex); // hex string six characters
  },

  // a simple converter stolen from google, only understands 6-digit values with or without #
  hexToRgb: (hex) => {
     // searces for # (optional) letters a-f and a number 3 times. 
     // "i" makes the search case-insensitive
    var rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return rgb ? { // returning the rgb-values in a pretty format
      r: parseInt(rgb[1], 16),
      g: parseInt(rgb[2], 16),
      b: parseInt(rgb[3], 16)
    } : null;
  }
}
